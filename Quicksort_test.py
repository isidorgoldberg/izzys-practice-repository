#!/usr/bin/python


import Quicksort_inplace
reload(Quicksort_inplace)
from Quicksort_inplace import *
import pytest

def simple_test():
    sort_algorithms = [(func_quicksort, "Quick sort functional"), (quicksort, "Quick sort in place")]
    for sort_algorithm, name in sort_algorithms:
        
        assert sort_algorithm([4,5,1,3,2]) == [1,2,3,4,5], "%s failed." % name
        #test sorting numbers with different digit lenghts
        assert sort_algorithm([1,100,10,50,1000]) == [1,10,50,100,1000], "%s failed." % name
        #test sorting numbers with different signs
        assert sort_algorithm([4,-5,1,3,2]) == [-5,1,2,3,4], "%s failed." % name
        #test sorting numbers with different lengths and signs
        assert sort_algorithm([1,123,-10,57,-1008,0]) == [-1008,-10,0,1,57,123], "%s failed." % name
        #test sorting an empty list
        assert sort_algorithm([])==[], "%s failed." % name
        #test sorting a list with just one element
        assert sort_algorithm([0])==[0], "%s failed." % name
        #test sorting a list with repeated elements
        assert sort_algorithm([-1,0,1,1,-1])==[-1,-1,0,1,1], "%s failed." % name
def exception_test():
    sort_algorithms = [(func_quicksort, "Quick sort functional"), (quicksort, "Quick sort in place")]
    for sort_algorithm, name in sort_algorithms:
        with pytest.raises(InvalidInputException):
            sort_algorithm(None)

def get_tests():
  
    return [simple_test, exception_test]

if __name__ == '__main__' :
    print 'Running tests...'
    for test in get_tests():
        test()
    print 'All tests passed!'
