#!usr/bin/python

import random
import operator

class InvalidInputException(Exception):
    """docstring for InvalidInputException"""
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)
        

def lrange(num1, num2 = None, step = 1):
    op = operator.__lt__

    if num2 is None:
        num1, num2 = 0, num1
    if num2 < num1:
        if step > 0:
            num1 = num2
        op = operator.__gt__
    elif step < 0:
        num1 = num2

    while op(num1, num2):
        yield num1
        num1 += step

def partition(A, low, hi):
    """
    partition: array, int, int--> None
    Purpose: partitions a subarray of A with respect to a random pivot
    """
    #Choose a pivot
    pivotIndex = low+ (hi-low)//2
    pivotVal = A[pivotIndex]
    #place the pivot at the end of the array
    swap(A, pivotIndex, hi)
    #set the insertion index to low (where we will begin the traversal)   
    curr = low
    #for all values in the array between hi and low (inclusive) 
    for i in lrange(low, hi):
        #if the value is less than the pivot, place it towards the front
        #of the array (at the insertion index), and increment the insertion index
        if A[i] <= pivotVal:
            swap(A,curr,i)
            curr += 1
    #replace the pivot
    swap(A, curr, hi)
    #return the insertion index/index of the pivot
    return curr

def swap(A, i1, i2):
    """
    Swap: Array, int, int--> None
    Purpose: swap the values at two indices in an array
    """
    temp = A[i1]
    A[i1] = A[i2]
    A[i2] = temp


def quicksort(A):
    """
    quicksort: array--> array
    Purpose: sorts an array
    """
    #If a null array is passed in, raise an exception
    if A is None:
        raise InvalidInputException('null input')
    #Create a stack of indices for the starts and ends of array partitions
    stack = [len(A)-1, 0]
    #while there are un partitioned subarrays
    while len(stack) > 0:
        low = stack.pop()
        hi = stack.pop()
        if low < hi:
            #partition the subarray
            pivotIndex = partition(A, low, hi)
            #append the hi and low indices around the pivot to the stack
            stack.append(pivotIndex-1)
            stack.append(low)
            stack.append(hi)
            stack.append(pivotIndex + 1)
    return A

def func_quicksort(A):
    """
    func_quicksort: array--> array
    Purpose: sorts an array
    """
    #if a null array is passed in, raise an exception
    if A is None:
        raise InvalidInputException('null input')
    #if the array is empty or contains one element, return it
    if len(A) <= 1:
        return A
    subarrays = []
    sorted = []
    #append the whole array to a stack of subarrays
    subarrays.append((A,False))
    #while there are unsorted subarrays
    while len(subarrays) > 0:
        #pop a subarray
        arr = subarrays.pop()
        #if the array contains only repeated elements, add it to the sorted array
        if arr[1]:
            sorted = sorted + arr[0]
        #otherwise, break the array into 3 subarrays, holding elements less than,
        #equal to, or greater than a random pivot respectively and add them to 
        #the stack if they are not empty
        else:
            piv = arr[0][len(arr[0])//2]
            L = filter(lambda x: x < piv, arr[0])
            E = filter(lambda x: x == piv, arr[0])
            G = filter(lambda x: x > piv, arr[0])
            if len(G) > 0:
                subarrays.append((G,False))
            if len(E) > 0:
                subarrays.append((E,True))
            if len(L) > 0:
                subarrays.append((L,False))
    return sorted
