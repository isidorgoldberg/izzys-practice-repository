README
Design choices:
These files include two iterative quick sort functions, which sort an input array.
Both use an explicit stack to model recursion, bypassing the limits of recursion
depth for use on large arrays. One is in place, while the other makes use of filter
to enable a less memory efficient, but more elegant semi-functional approach. These
sorts ONLY work for homogeneous data sets where values are comparable using <,>,==.

Known bugs:
None

Tests:
Included is a test file which tests various edge cases on small arrays, along with
a test to ensure that the sorts handle null inputs.